﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace np_lesson4_client
{
    class Program
    {
        static void Main(string[] args)
        {
            Socket client = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            string str = "Hello User!";
            string ipSrv = "127.0.0.1";
            int port = 12345;

           int sendSize = client.SendTo(Encoding.UTF8.GetBytes(str), new IPEndPoint(IPAddress.Parse(ipSrv), port));
            Console.WriteLine($"Send size: {sendSize}");
            Console.ReadLine();
        }
    }
}
